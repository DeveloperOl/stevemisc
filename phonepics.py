#!/usr/bin/env python3
#
# phonepics - convert your pics for a phone friendly file structure
# with sharing support for nextcloud

# Copyright (C) 2022 skrodzki@stevekist.de
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from typing import List
import configargparse
import os
import os.path
import subprocess
import sys
import json
import re

# uses: https://github.com/Dosugamea/NEXT-OCS-API-forPy just put it in the same folder

from NextCloud import NextCloud


def read_from_file(filepath):
    """
    reads data from file
    :param filepath:
    :return: data read from file
    """
    if os.path.isfile(filepath):
        with open(filepath) as file:
            data = file.read()
            return data
    else:
        print(f"File '{filepath}' is not existing")
        return None

def nextcloud_set_share(newshare, path):
    if nxc is not None:
        shareset = False
        for share in sharelist["ocs"]["data"]:
            #print(share["path"], ":", share["share_with"])
            if share["path"] == path and share["share_with"] == newshare:
                print(f"Nextcloud Share for {newshare} already set on {path}")
                shareset = True
        if not shareset:
            if args.nothing:
                print(f"Would create Nextcloud Share for {newshare} on {path}")
            else:
                print(f"Create Nextcloud Share for {newshare} on {path}")
                nxc.createShare(path,1,shareWith=newshare)    

def processdir(aktpath):
    # print ("checking dir:",entry,"\n")
    createpath = ""
    entries = os.listdir(os.path.join(sourcepath, aktpath))
    subpathsingle = "_".join(aktpath.split(os.sep))    
    destdir = os.path.join(destpath,subpathsingle)

    for f in entries:
        srcfile = os.path.join(sourcepath,aktpath,f)
        # print("Checking: ", srcfile)
        if os.path.isfile(srcfile) and (re.match("jpg", f[-3:], re.IGNORECASE) or re.match("png", f[-3:], re.IGNORECASE)):
            destfile = os.path.join(destdir,f)
            destlist.append(destfile) ## add the file to the list, which will be checked later for deletion
            if (not os.path.isfile(destfile)): ## check, if destinationfile already exists
                if (not os.path.isdir(destdir)): ## check if destination dir already exists
                    if (args.nothing):                    
                        print("Would create dir: ",destdir)
                    else:
                        print("Creating dir: ",destdir)
                        os.mkdir(destdir)
                if (not args.nothing):                    
                    print("Converting: ", srcfile," -> ", destfile)
                    subprocess.call(f"convert -gamma 1.3 -resize {resize} "+srcfile+" "+destfile, shell=True)
                else:
                    print("Would convert: ", srcfile," -> ", destfile)
                    
        if os.path.isdir(srcfile):
            processdir(os.path.join(aktpath,f))
    if ".ncshare" in entries:
        sharedwith = read_from_file(os.path.join(sourcepath,aktpath,".ncshare"))
        sharedwith = sharedwith.rstrip()
        #print(f"Dir {destdir} will be shared with {sharedwith}")
        nextcloud_set_share(sharedwith, "/"+args.ncbasepath+"/"+subpathsingle)



mydir = os.path.expanduser("~/.config/phonepics/")
destlist: List[str] = []
resize = "1920x1080"
parser = configargparse.ArgumentParser(description='Convert your pictures to a flat hierarchy with nextcloud and lespas support', default_config_files = [os.path.join(mydir, 'config')])

parser.add_argument('-n', '--nothing', help='do nothing', action='store_true')
parser.add_argument('-s', '--source', help='the source path', required=True)
parser.add_argument('-d', '--destination', help='the destination path', required=True)
parser.add_argument('--resize', help='resize string (default 1920x1080)')
parser.add_argument('--ncurl', help='nextcloud base url')
parser.add_argument('--ncbasepath', help='nextcloud base path')
parser.add_argument('--ncuser', help='nextcloud user')
parser.add_argument('--ncpasswd', help='nextcloud password')


args = parser.parse_args()

if args.resize:
    resize = args.resize

if args.ncurl and args.ncbasepath and args.ncuser and args.ncpasswd:
    nxc = NextCloud(args.ncurl, args.ncuser, args.ncpasswd, True)
    sharelist = nxc.getShares()
    #print(sharelist)

yetdone = 0

## main

sourcepath = os.path.abspath(args.source)
destpath = os.path.abspath(args.destination)

num = 0
processdir("")

## now check if there are files that should be deleted

for (dirpath, dirnames, filenames) in os.walk(destpath):
    for f in filenames:
        lookfile = os.path.join(dirpath, f) # this is the file we found
        if (destlist.count(lookfile) == 0):
            if lookfile[-4:] == "json":
                print("Ignore json file (from lespas?):", lookfile)
            else:
                if (args.nothing):
                    print("Would delete:",lookfile)
                else:
                    print("Deleting:",lookfile)
                    os.remove(lookfile)

print("Total pictures:",len(destlist))

