# stevemisc

Miscellaneous stuff which helps me and perhaps you?


## digiimport.py
`digiimport.py` is a short script to move your pictures - with no content changed at all, not exif not data - to a collection structured by YYYY/MM and optional event.

### My Workflow looks like this: 

I have on my PC a folder called "/home/steve/pictures/incoming" and my collection should be under "/home/steve/pictures/collection". Then I start in the folder incoming this script with (assume it is in your path) `digiimport.py -d /home/steve/pictures/collection` and the magic starts. The script detects EXIF dates, Signal picture naming conventions, What's app naming conventions, android generic naming conventions and gets the date the pictures was made from that. The picture will then be renamed to YYYYMMDD_HHMMSS and moved to a folder inside your collection called YYYY/MM.

Give it a try, maybe it helps you. 

## phonepics.py

`phonepics.py` is my script to convert my nested folder hierarchy with my pictures to a flat folder structure and resized pictures. Only files ending with .jpg and .png are converted.

It is designed to provide the right structure for the famous FOSS library viewer Les Pas: https://github.com/scubajeff/lespas

By this, I am able to sync all my pictures with my mobile phone: while the local collection eats up around 29G, the reduced sizes dir needs 6,2G... 

You have also the possibility to share a folder with a Nextcloud group by writing the Name of this Group in  the folder in a file called `.ncshare`

For installation, put the script in your bin path and put also the NextCloud.py library from https://github.com/Dosugamea/NEXT-OCS-API-forPy there.

Start the script with: 

phonepics.py -s source/path/ -d destination/path --ncurl https://nextcloud.example.com --ncuser theuser --ncpasswd joshua42 --ncbasepath lespas

starting with -n would show you, what the script would do. 
The --nc* parameters are only necessary if you want to use Nextcloud shares. 
Perhaps try it first with just a few folders, which you copy to a test location.

Hint: The users of the group which get the shares, will have to move their shared folders also to an own lespas folder.

## More Misc

`drexteil.sh` is a script for restarting a device which is not pingable anymore just by switching off and on a wifi plug.

`delete_alexa_devices.robot` is a robot script for deleting all smart home devices from your alexa account as it is not possible to use the delete all button. You can start it  with `robot -v USERNAME:your_amazon_account -v PASSWORD:your_amazon_password delete_alexa_devices.robot`
