*** Settings ***
Documentation          Delete all smart home devices from alexa

Library                SeleniumLibrary

*** Variables ***
${USERNAME}            amazonaccount
${PASSWORD}            amazonpassword
${HEADLESS}            False
${URL}                 https://alexa.amazon.de
${BROWSER}             Chrome
${DELAY}               2
${TIMEOUT}             20

*** Keywords ***

Open Chrome
    [Arguments]    ${url}=${URL}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Run Keyword If 	${HEADLESS} == True  Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    ${browser_index}    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size  1280   1024
    Set Selenium Speed   ${DELAY}
    Set Selenium Timeout  ${TIMEOUT}
    Goto   ${url}
    [Return]        ${browser_index}

*** Test Cases ***

Login
    Open Chrome
    Input Text            email   ${USERNAME}
    Input Password        password   ${PASSWORD}
    ${vertical}           Get Vertical Position   xpath://input[@name="rememberMe"]
    ${horizontal}         Get Horizontal Position   xpath://input[@name="rememberMe"]
    #Click Element         xpath://i
    Click Element At Coordinates         xpath://input[@name="rememberMe"]    0   0
    Click Element         xpath://input[@id="signInSubmit"]     

Goto Devices
     Page Should Contain  Smart Home
     Click Element        xpath: //dd[@id="iSmartHome"]
     Page Should Contain  Devices
     Click Element        xpath: //dd[@id="gotoDevices"]

Remove Devices
     FOR   ${i}    IN RANGE    9999999
        Log To Console    Loop ${i}
        Click Element     xpath://button[@id="connected-home-delete-button"]
        Wait Until Page Contains   Remove this appliance
        Click Element     xpath://button[@class="d-modal-button primary accept"]
     END